# .NET (with Mono) Docker images

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Docker Stars][docker-stars-badge]][docker-repository]
[![Docker Pulls][docker-pulls-badge]][docker-repository]

Docker images with .NET Core and Mono, based on Debian Linux.

Images are based on [my dotnet images][docker-pommalabs-dotnet] and are enriched with official [Mono][mono-website] packages (`mono-complete` meta-package for SDK and `mono-runtime` for ASP.NET runtime).

Runtime images follow a stricter security approach than [official dotnet images][docker-microsoft-dotnet],
as they are configured to run with an unprivileged user.

## Table of Contents

- [Tags](#tags)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Building Docker images](#building-docker-images)
- [License](#license)

## Tags

Following tags are available on [Docker Hub][docker-repository]:

| Tag name                 | WORKDIR    | dotnet base image | Mono   | End of life |
|--------------------------|------------|-------------------|--------|-------------|
| `latest`                 | `/opt/sln` | `6-sdk`           | `6.12` | 11/2024     |
| `dotnet-6-mono-6-aspnet` | `/opt/app` | `6-aspnet`        | `6.12` | 11/2024     |
| `dotnet-6-mono-6-sdk`    | `/opt/sln` | `6-sdk`           | `6.12` | 11/2024     |
| `dotnet-5-mono-6-aspnet` | `/opt/app` | `5-aspnet`        | `6.12` | 05/2022     |
| `dotnet-5-mono-6-sdk`    | `/opt/sln` | `5-sdk`           | `6.12` | 05/2022     |
| `dotnet-3-mono-6-aspnet` | `/opt/app` | `3-aspnet`        | `6.12` | 12/2022     |
| `dotnet-3-mono-6-sdk`    | `/opt/sln` | `3-sdk`           | `6.12` | 12/2022     |

Tags ending with `-sdk` suffix should be used to build .NET Core and .NET Framework applications,
while tags ending `-aspnet` suffix should be used at runtime.

`latest` tag is an SDK image which always contains latest stable .NET and Mono releases.

Tags are rebuilt every week by a job scheduled on GitLab CI platform,
which also performs some automated testing of the image integrity.

## Usage

Images with .NET Core and Mono can be useful in order to build applications and libraries
which need to target both .NET Core and .NET Framework.

For example, the following block defines a build step on GitLab CI using the SDK image:

```yaml
dotnet-build:
  stage: build
  image: pommalabs/dotnet-mono:dotnet-3-mono-6-sdk
  script:
  - dotnet build MyProjectThatTargetsMultipleFrameworks.csproj
```

In order for multi-targeting to work, the project needs to reference a `netfx.props`
file which sets the required configuration up.
Andrew Lock wrote a [very useful article][andrewlock-netfx-props] on this topic,
while I maintain an [updated version of that file][gitlab-netfx-props] for my personal projects.

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.
I replaced the __Install__ section with __Tags__, since I thought that it made no sense
to "install" an helper Docker image.

### Building Docker images

Docker images can be built with following command:

```bash
while IFS=: read -r path tag
do
    docker build . -f $path/Dockerfile -t $tag
done < ".dockertags"
```

## License

MIT © 2019-2022 [Alessio Parma][personal-website]

[andrewlock-netfx-props]: https://andrewlock.net/building-net-framework-asp-net-core-apps-on-linux-using-mono-and-the-net-cli/
[docker-microsoft-dotnet]: https://hub.docker.com/r/microsoft/dotnet/
[docker-pommalabs-dotnet]: https://hub.docker.com/r/pommalabs/dotnet/
[docker-pulls-badge]: https://img.shields.io/docker/pulls/pommalabs/dotnet-mono?style=flat-square
[docker-repository]: https://hub.docker.com/r/pommalabs/dotnet-mono
[docker-stars-badge]: https://img.shields.io/docker/stars/pommalabs/dotnet-mono?style=flat-square
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-netfx-props]: https://gitlab.com/pommalabs/mime-types/-/blob/main/netfx.props
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/docker/dotnet-mono/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/docker/dotnet-mono/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[mono-website]: https://www.mono-project.com/
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[personal-website]: https://alessioparma.xyz/
[project-license]: https://gitlab.com/pommalabs/docker/dotnet-mono/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
